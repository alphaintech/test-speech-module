# -*- coding: utf-8 -*-
from __future__ import absolute_import

import os
import logging
import click
import importlib
from asrclient import client
from PyQt4.QtCore import pyqtSlot
from SpeechFunctions import *
import sys
from PyQt4.QtGui import *
import pyglet
import pygame
from pydub import AudioSegment
import codecs
import subprocess
import threading, time
reload(sys)
sys.setdefaultencoding('utf-8')

if __name__ == '__main__':

    app = QApplication(sys.argv)

    widget = QWidget()
    widget.resize(640, 480)
    widget.setWindowTitle("SpeechApplication")

    tbTextToSpeech = QTextEdit(widget)
    # tbTextToSpeech.resize(280,150)

    formLayout = QFormLayout()
    formLayout.setSpacing(10)
    formLayout.addRow(QLabel("Speech apl"), tbTextToSpeech)

    recBtn = QPushButton("Start speech recognition", widget)
    recBtn.setToolTip(("Start speech recognition"))
    formLayout.addRow(recBtn)

    loadBtn = QPushButton("Load text", widget)
    loadBtn.setToolTip(("Load text"))
    formLayout.addRow(loadBtn)

    ttsBtn = QPushButton("Synthes text to speech", widget)
    ttsBtn.setToolTip(("Synthes text to speech"))

    @pyqtSlot()
    def on_click_tts():
        textToSpeech = tbTextToSpeech.toPlainText()
        speechSynthesizer = SpeechSynthesizer()
        speechSynthesizer.synthesText(textToSpeech)

    ttsBtn.clicked.connect(on_click_tts)
    # getTextBtn.resize(getTextBtn.sizeHint())
    # button.move(20, 100)
    formLayout.addRow(ttsBtn)

    reformatSoundBtn = QPushButton("Reformat to WAVE file", widget)
    reformatSoundBtn.setToolTip("Reformat to WAVE file")

    @pyqtSlot()
    def on_click_load():

        inputFile = codecs.open('text.txt', 'r', encoding='utf-8')
        words = inputFile.readline()
        tbTextToSpeech.append(words)

    loadBtn.clicked.connect(on_click_load)

    @pyqtSlot()
    def on_click_rec():

        def thr():
            chunks = []
            chunks = client.read_chunks_from_pyaudio(client.DEFAULT_CHUNK_SIZE_VALUE)

            def default_callback(utterance, start_time=0.0, end_time=0.0, data=None):
                f = open('text.txt', 'w')
                f.write(utterance)

            client.recognize(chunks, callback=default_callback)

        rec = threading.Thread(name='rec', target=thr)
        rec.start()
    recBtn.clicked.connect(on_click_rec)

    @pyqtSlot()
    def on_click_reformat_to_wav():
        # Get filename using QFileDialog
        filePath = QFileDialog.getOpenFileName(widget, 'Reformat File', '/', "Music Files (*.mp3 *.wav *.flac)")
        # filePath = str(filePath)
        fileWorker = FileWorker()
        fileWorker.reformatToWavFile(filePath)

    reformatSoundBtn.clicked.connect(on_click_reformat_to_wav)

    playSoundBtn = QPushButton("Play WAVE file", widget)
    playSoundBtn.setToolTip("Play WAVE file")

    @pyqtSlot()
    def on_click_play_wav_file():
        filePath = QFileDialog.getOpenFileName(widget, 'Open File', '/', "Music Files (*.wav)")
        fileWorker = FileWorker()
        fileWorker.playWavFile(filePath)

    playSoundBtn.clicked.connect(on_click_play_wav_file)

    formLayout.addRow(reformatSoundBtn, playSoundBtn)
    widget.setLayout(formLayout)

    widget.show()
    app.exec_()
    sys.exit(0)



